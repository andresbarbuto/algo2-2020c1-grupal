#include "aed2_Mapa.h"

Mapa::Mapa() {
    _primero = NULL;
    _ultimo = NULL;
}

Mapa::Rio::Rio(Direccion d, const int &pos) {
    if (d == Vertical) {
        esVertical = true;
    } else {
        esVertical = false;
    }

    posicion = pos;
    siguiente = NULL;
}

void Mapa::agregarRio(Direccion d, int p) {
    shared_ptr<Rio> nuevo(new Rio(d, p));

    if (_primero == NULL) {
        _primero = nuevo;
    } else {
        _ultimo->siguiente = nuevo;
    }

    _ultimo = nuevo;
}

void Mapa::unirMapa(const Mapa& m2) {
    if (m2._primero != NULL) {
        if (_primero == NULL) {
            _primero = m2._primero;
            _ultimo = m2._ultimo;
        }
        else {
            _ultimo->siguiente = m2._primero;
            _ultimo = m2._ultimo;
        }
    }
}

bool Mapa::hayRio(Casilla c) const {

    if (_primero == NULL) {
        return false;
    }

    shared_ptr<Rio> actual = _primero;

    while (actual != NULL) {
        if (((actual->esVertical && actual->posicion == c.first) || (!(actual->esVertical) && actual->posicion == c.second))) {
            return true;
        }
        actual = actual->siguiente;
    }

    return false;
}

aed2_Mapa::aed2_Mapa() {
    Mapa m = Mapa();
    _mapa = m;
}

void aed2_Mapa::agregarRio(Direccion d, int p) {
    _mapa.agregarRio(d, p);
}

void aed2_Mapa::unirMapa(aed2_Mapa m2) {
    _mapa.unirMapa(m2.mapa());
}

bool aed2_Mapa::hayRio(Casilla c) const {
    return _mapa.hayRio(c);
}

Mapa aed2_Mapa::mapa() {
    return _mapa;
}