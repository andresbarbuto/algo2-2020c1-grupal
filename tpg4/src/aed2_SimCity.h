#ifndef AED2_SIMCITY_H
#define AED2_SIMCITY_H

#include <memory>
#include <iostream>
#include <set>
#include <vector>
#include "aed2_Mapa.h"

using namespace std;

using Nat = unsigned int;

class SimCity {
public:
    SimCity(Mapa m);
    void agregarCasa(Casilla c);
    void agregarComercio(Casilla c);
    void avanzarTurno();
    void unir(const SimCity& sc);
    Mapa mapa() const;
    set<Casilla> casas() const;
    set<Casilla> comercios() const;
    Nat nivel(Casilla c) const;
    bool huboConstruccion() const;
    Nat popularidad() const;
    Nat antiguedad() const;

private:
    Mapa _mapa;
    Nat _antiguedad;
    Nat _popularidad;
    bool _huboConstruccion;

    struct Construccion {
        Construccion(bool esCasa, Casilla c);
        Casilla posicion;
        bool esCasa;
        Nat nivel;
        shared_ptr<Construccion> siguiente;
    };

    shared_ptr<Construccion> _primera;
    shared_ptr<Construccion> _ultima;

    Nat distanciaManhattan(Casilla c1, Casilla c2) const;

    int minimo(int a, int b) const;
    int maximo(int a, int b) const;
    
};

class aed2_SimCity {
public:

    // Generadores:

    aed2_SimCity(aed2_Mapa m);

    // Precondicion: Se puede construir en la Casilla c
    void agregarCasa(Casilla c);

    // Precondicion: Se puede construir en la Casilla c
    void agregarComercio(Casilla c);

    // Precondicion: Hubo construccion en el turno actual
    void avanzarTurno();

    // Precondicion: No se solapan las construcciones con los rios
    //  ni las construcciones de nivel maximo de ambas partidas
    void unir(aed2_SimCity sc);

    // Observadores:

    aed2_Mapa mapa() const;

    set<Casilla> casas() const;

    set<Casilla> comercios() const;

    // Precondicion: Hay construccion en la Casilla p
    Nat nivel(Casilla c) const;

    bool huboConstruccion() const;

    Nat popularidad() const;

    Nat antiguedad() const;

    // Conversiones
    
    // Esta función sirve para convertir del SimCity de la cátedra al SimCity
    // implementado por ustedes
    SimCity simCity();

private:
    SimCity _SimCity;
    aed2_Mapa _mapa;
};

#endif // AED2_SIMCITY_H
