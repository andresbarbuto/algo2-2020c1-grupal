#ifndef AED2_MAPA_H
#define AED2_MAPA_H

#include <memory>
#include <iostream>
#include "Tipos.h"

using namespace std;

class Mapa {
public:
    Mapa();
    void agregarRio(Direccion d, int p);
    void unirMapa(const Mapa& m2);
    bool hayRio(Casilla c) const;

private:
    struct Rio {
        Rio(Direccion d, const int& posicion);
        bool esVertical;
        int posicion;
        shared_ptr<Rio> siguiente;
    };

    shared_ptr<Rio> _primero;
    shared_ptr<Rio> _ultimo;
};


class aed2_Mapa {
public:

    // Generadores:

    aed2_Mapa();

    void agregarRio(Direccion d, int p);

    // Observadores:

    bool hayRio(Casilla c) const;

    // Otras operaciones:

    void unirMapa(aed2_Mapa m2);

    // Conversiones
    
    // Esta función sirve para convertir del Mapa implementado por ustedes
    // a la clase Mapa de la cátedra
    // aed2_Mapa(Mapa m);

    // Esta función sirve para convertir del Mapa de la cátedra (aed2_Mapa)
    // a la clase Mapa definida por ustedes
    Mapa mapa();

private:
    Mapa _mapa;
};

#endif // AED2_MAPA_H
