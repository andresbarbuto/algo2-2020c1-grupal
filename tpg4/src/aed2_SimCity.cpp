#include "aed2_SimCity.h"

// Completar
SimCity::SimCity(Mapa m) {
    _mapa = m;
    _antiguedad = 0;
    _popularidad = 0;
    _huboConstruccion = false;
    _primera = NULL;
    _ultima = NULL;
}

void SimCity::agregarCasa(Casilla c) {
    shared_ptr<Construccion> nueva(new Construccion(true, c));

    if (_primera == NULL) {
        _primera = nueva;
        _ultima = nueva;

    } else {
        _ultima->siguiente = nueva;
        _ultima = nueva;
    }

    _huboConstruccion = true;
}

void SimCity::agregarComercio(Casilla c) {
    shared_ptr<Construccion> nueva(new Construccion(false, c));

    if (_primera == NULL) {
        _primera = nueva;
        _ultima = nueva;

    } else {
        _ultima->siguiente = nueva;
        _ultima = nueva;
    }

    _huboConstruccion = true;
}

void SimCity::avanzarTurno() {
    _antiguedad++;
    _huboConstruccion = false;

    shared_ptr<Construccion> actual = _primera;

    while (actual != NULL) {
        actual->nivel++;
        actual = actual->siguiente;
    }
}

void SimCity::unir(const SimCity& sc) {
    _mapa.unirMapa(sc._mapa);

    if (sc._primera != NULL) {
        if (_primera == NULL) {
            _primera = sc._primera;
            _ultima = sc._ultima;
        } else {
            _ultima->siguiente = sc._primera;
            _ultima = sc._ultima;
        }
    }

    _popularidad = _popularidad + sc._popularidad + 1;

    _huboConstruccion = _huboConstruccion || sc._huboConstruccion;
    _antiguedad = maximo(_antiguedad, sc._antiguedad);

}

Mapa SimCity::mapa() const {
    return _mapa;
}

set<Casilla> SimCity::casas() const {
    set<Casilla> res;

    if (_primera == NULL) {
        return res;
    }

    shared_ptr<Construccion> actual = _primera;

    while (actual != NULL) {
        if (actual->esCasa) {
            res.insert(actual->posicion);
        }
        actual = actual->siguiente;
    }

    return res;
}

set<Casilla> SimCity::comercios() const {
    set<Casilla> res;

    if (_primera == NULL) {
        return res;
    }

    set<Casilla> c = casas();

    shared_ptr<Construccion> actual = _primera;

    while (actual != NULL) {
        if (!(actual->esCasa)) {
            if (c.count(actual->posicion) == 0) {
                res.insert(actual->posicion);
            }

        }
        actual = actual->siguiente;
    }

    return res;
}

Nat SimCity::nivel(Casilla c) const {
    bool hayCasa = false;
    Nat maxCasa = 0;
    Nat maxComercio = 0;

    shared_ptr<Construccion> actual = _primera;

    while (actual != NULL) {
        if (actual->esCasa && distanciaManhattan(actual->posicion, c) <= 3) {
            maxComercio = maximo(maxComercio, actual->nivel);
        }
        if (actual->posicion == c) {
            if (actual->esCasa) {
                hayCasa = true;
                maxCasa = maximo(maxCasa, actual->nivel);
            } else {
                maxComercio = maximo(maxComercio, actual->nivel);
            }
        }
        actual = actual->siguiente;
    }

    if (hayCasa) {
        return maxCasa;
    } else {
        return maxComercio;
    }
}

bool SimCity::huboConstruccion() const {
    return _huboConstruccion;
}

Nat SimCity::popularidad() const {
    return _popularidad;
}

Nat SimCity::antiguedad() const {
    return _antiguedad;
}

SimCity::Construccion::Construccion(bool esCasa, Casilla c):esCasa(esCasa), posicion(c) {
    nivel = 0;
    siguiente = NULL;
}

Nat SimCity::distanciaManhattan(Casilla c1, Casilla c2) const {
    return abs(c1.first - c2.first) + abs(c1.second - c2.second);
}

int SimCity::maximo(int a, int b) const {
    if (a > b) {
        return a;
    } else {
        return b;
    }
}

aed2_SimCity::aed2_SimCity(aed2_Mapa m):_SimCity(SimCity(m.mapa())), _mapa(m) {}

SimCity aed2_SimCity::simCity() {
    return _SimCity;
}

void aed2_SimCity::agregarCasa(Casilla c) {
    _SimCity.agregarCasa(c);
}

void aed2_SimCity::agregarComercio(Casilla c) {
    _SimCity.agregarComercio(c);
}

void aed2_SimCity::avanzarTurno() {
    _SimCity.avanzarTurno();
}

void aed2_SimCity::unir(aed2_SimCity sc) {
    _SimCity.unir(sc._SimCity);
}

aed2_Mapa aed2_SimCity::mapa() const {
    return _mapa;
}

set<Casilla> aed2_SimCity::casas() const {
    return _SimCity.casas();
}

set<Casilla> aed2_SimCity::comercios() const {
    return _SimCity.comercios();
}

Nat aed2_SimCity::nivel(Casilla c) const {
    return _SimCity.nivel(c);
}

bool aed2_SimCity::huboConstruccion() const {
    return _SimCity.huboConstruccion();
}

Nat aed2_SimCity::popularidad() const {
    return _SimCity.popularidad();
}

Nat aed2_SimCity::antiguedad() const {
    return _SimCity.antiguedad();
}